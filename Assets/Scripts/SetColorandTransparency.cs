﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;
using UtyMap.Unity;
using UtyMap.Unity.Data;
using UtyMap.Unity.Infrastructure.Diagnostic;
using UtyMap.Unity.Infrastructure.Primitives;
using UtyMap.Unity.Utils;
using UtyRx;

public class SetColorandTransparency : MonoBehaviour {
	static int min = 10000;
	static int max = 0;
	static int[] mindate = new int[31];
	static int[] maxdate = new int[31];
	static Color[,] staticColor;
	static float[,] staticTransparency;
	static Color[,,] staticColorDate;
	static float[,,] staticTransparencyDate;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public static Color[,] GetColor(int[,] aggregate_result){
		min = 10000;
		max = 0;
		for (int i = 0; i < aggregate_result.GetLength(0); i++) {
			for (int j = 0; j < aggregate_result.GetLength(1); j++) {
				if (min > aggregate_result[i,j]) {
					min = aggregate_result [i,j];
				}
				if (max < aggregate_result[i,j]) {
					max = aggregate_result [i,j];
				}
			}
		}

		Color[,] colors = new Color[aggregate_result.GetLength(0), aggregate_result.GetLength(1)];
		staticColor = new Color[aggregate_result.GetLength(0), aggregate_result.GetLength(1)];
		for (int i = 0; i < aggregate_result.GetLength(0); i++) {
			for (int j = 0; j < aggregate_result.GetLength(1); j++) {
				float value = (float)(aggregate_result[i,j] - min)/(float)(max - min);
				float hue = (1.0f - value) * 160.0f / 240.0f;
				colors[i,j] = Color.HSVToRGB(hue, 1.0f, 1.0f);
				staticColor[i,j] = Color.HSVToRGB(hue, 1.0f, 1.0f);
			}
		}
		return colors;
	}

	public static float[,] GetTransparency(int[,] aggregate_result){
		float[,] transparency = new float[aggregate_result.GetLength(0), aggregate_result.GetLength(1)];
		staticTransparency = new float[aggregate_result.GetLength(0), aggregate_result.GetLength(1)];
		for (int i = 0; i < aggregate_result.GetLength(0); i++) {
			for (int j = 0; j < aggregate_result.GetLength(1); j++) {
				float value = (float)(aggregate_result [i,j] - min) / (float)(max - min);
				transparency [i,j] = (float)Math.Pow ((double)value, 1.2);
				staticTransparency[i,j] = (float)Math.Pow ((double)value, 1.2);
			}
		}
		return transparency;
	}


	public static Color[,,] GetColorDate(int[,,] aggregate_result){
		for (int i = 0; i < 31; i++) {
			mindate [i] = 10000;
			maxdate [i] = 0;
		}
		for (int k = 0; k < aggregate_result.GetLength (0); k++) {
			for (int i = 0; i < aggregate_result.GetLength (1); i++) {
				for (int j = 0; j < aggregate_result.GetLength (2); j++) {
					if (mindate[k] > aggregate_result [k, i, j]) {
						mindate[k] = aggregate_result [k, i, j];
					}
					if (maxdate[k] < aggregate_result [k, i, j]) {
						maxdate[k] = aggregate_result [k, i, j];
					}
				}
			}
		}

		Color[,,] colors = new Color[aggregate_result.GetLength (0), aggregate_result.GetLength(1), aggregate_result.GetLength(2)];
		staticColorDate = new Color[aggregate_result.GetLength (0), aggregate_result.GetLength(1), aggregate_result.GetLength(2)];
		for (int k = 0; k < aggregate_result.GetLength (0); k++) {
			for (int i = 0; i < aggregate_result.GetLength (1); i++) {
				for (int j = 0; j < aggregate_result.GetLength (2); j++) {
					float value = (float)(aggregate_result [k, i, j] - mindate[k]) / (float)(maxdate[k] - mindate[k]);
					float hue = (1.0f - value) * 160.0f / 240.0f;
					colors [k, i, j] = Color.HSVToRGB (hue, 1.0f, 1.0f);
					staticColorDate [k, i, j] = Color.HSVToRGB (hue, 1.0f, 1.0f);
				}
			}
		}
		return colors;
	}

	public static float[,,] GetTransparencyDate(int[,,] aggregate_result){
		float[,,] transparency = new float[aggregate_result.GetLength(0), aggregate_result.GetLength(1), aggregate_result.GetLength(2)];
		staticTransparencyDate = new float[aggregate_result.GetLength(0), aggregate_result.GetLength(1), aggregate_result.GetLength(2)];
		for (int k = 0; k < aggregate_result.GetLength (0); k++) {
			for (int i = 0; i < aggregate_result.GetLength (1); i++) {
				for (int j = 0; j < aggregate_result.GetLength (2); j++) {
					float value = (float)(aggregate_result [k, i, j] - mindate[k]) / (float)(maxdate[k] - mindate[k]);
					transparency [k, i, j] = (float)Math.Pow ((double)value, 1.2);
					staticTransparencyDate [k, i, j] = (float)Math.Pow ((double)value, 1.2);
				}
			}
		}
		return transparency;
	}


	public static Color GetOneColor(int date, GameObject cube){
		string[] cubename = cube.name.Split (',');
		Color onecolor;
		if (date == -1) {
			onecolor = staticColor [Int32.Parse (cubename [0]), Int32.Parse (cubename [1])];
		} else {
			//index of date in array should be minused 1
			onecolor = staticColorDate [date-1, Int32.Parse (cubename [0]), Int32.Parse (cubename [1])];
		}
		return onecolor;
	}

	public static float GetOneTransparency(int date, GameObject cube){
		string[] cubename = cube.name.Split (',');
		float onetransparency;
		if (date == -1) {
			onetransparency = staticTransparency[Int32.Parse(cubename[0]),Int32.Parse(cubename[1])];
		} else {
			onetransparency = staticTransparencyDate [date-1, Int32.Parse (cubename [0]), Int32.Parse (cubename [1])];
		}
		return onetransparency;
	}
}
