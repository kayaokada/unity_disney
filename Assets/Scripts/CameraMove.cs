﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityStandardAssets.CrossPlatformInput;

public class CameraMove : MonoBehaviour {

	public float moveSpeed = 100.0f;

	private Camera camera;
	private CharacterController character;

	// Use this for initialization
	void Start () {
		camera = GetComponentInChildren<Camera> ();
		character = GetComponent<CharacterController> ();
	}
	
	// Update is called once per frame
	void Update () {
		float horizontalAxis = CrossPlatformInputManager.GetAxis ("Horizontal");
		float verticalAxis = CrossPlatformInputManager.GetAxis ("Vertical");

		Vector3 move = Vector3.zero;

		if (Mathf.Abs (verticalAxis) > Mathf.Epsilon) {
			move += camera.transform.forward * moveSpeed * verticalAxis * Time.deltaTime;

//			transform.Translate (camera.transform.forward * moveSpeed * verticalAxis * Time.deltaTime);
		}

		if (Mathf.Abs (horizontalAxis) > Mathf.Epsilon) {
			move += camera.transform.right * moveSpeed * horizontalAxis * Time.deltaTime;

//			transform.Translate (camera.transform.right * moveSpeed * horizontalAxis * Time.deltaTime);
		}

		character.Move (move);
	}
}
