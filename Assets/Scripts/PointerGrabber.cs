﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class PointerGrabber : MonoBehaviour {

	public Transform cameraTransform;
	private new Camera camera;
	private Transform attachedObject;
	private Transform minimapcube;
	private Transform slider;
	public GameObject particle;
	public float teleportdistance = 10000.0f;

	private bool isGripped;
	private static bool onhoverFlag;

	private Coroutine throwCoroutine;
	private Transform minimapcube_pre;

	// Use this for initialization
	void Start () {
		camera = cameraTransform.GetComponent<Camera> ();

		VRTK_Pointer vrtkpointer = GetComponent<VRTK_Pointer>();
		if (vrtkpointer != null) {
			vrtkpointer.DestinationMarkerHover += OnHover;
			vrtkpointer.DestinationMarkerEnter += OnCubeUsedHilighted;
			vrtkpointer.DestinationMarkerExit += OnCubeUsedUnHilighted;
		}

		VRTK_ControllerEvents controllerEvents = GetComponent<VRTK_ControllerEvents> ();
		if (controllerEvents != null) {
			controllerEvents.GripPressed += OnGripPressed;
			controllerEvents.GripReleased += OnGripRelease;
			controllerEvents.TriggerReleased += OnTriggerReleased;
		}

		VRTK_InteractGrab interactgrab = GetComponent<VRTK_InteractGrab> ();
		if (interactgrab != null) {
			interactgrab.ControllerGrabInteractableObject += OnGrab;
			interactgrab.ControllerUngrabInteractableObject += OnGrabRelease;
			//interactgrab.GrabButtonPressed += OnGrabPressed;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerReleased(object sender, ControllerInteractionEventArgs e)
	{
		CubeInteraction.SetGrabbingPanelFlag (false);
		onhoverFlag = false;

		if (attachedObject != null) {
			Vector3 position = attachedObject.transform.position;
			Quaternion rotation = attachedObject.transform.rotation;

			attachedObject.transform.SetParent (null, true);

			attachedObject.transform.position = position;
			attachedObject.transform.rotation = rotation;

			attachedObject = null;
		} else if (minimapcube != null) {
			minimapcube_pre = minimapcube;
			SteamVR_Fade.Start (Color.black, 0.3f);
			Invoke ("FadeIn", 0.5f);

		} else if (slider != null) {
			VRTK_InteractGrab grab = GetComponent<VRTK_InteractGrab> ();
			if (grab != null) {
				grab.ForceRelease ();
			}

			VRTK_InteractableObject interactable = slider.GetComponent<VRTK_InteractableObject> ();
			if (interactable != null) {
				interactable.ToggleHighlight (false);
			}

			slider = null;
			GameObject[] panels = GameObject.FindGameObjectsWithTag ("Panel");
			foreach (GameObject panel in panels) {
				Destroy (panel);
			}
			int value = ScaleChange.GetCurrentValue ();
			Cube_instance.largecubecolor (value);
		}
	}

	private void FadeIn(){
		GameObject camera_root = GameObject.FindWithTag ("VRCamera");
		GameObject camera_eye = GameObject.FindWithTag ("MainCamera");
		Vector3 eyelocalspace = minimapcube_pre.parent.worldToLocalMatrix.MultiplyPoint (camera_eye.transform.position);
		Vector3 eye_to_smallcube = minimapcube_pre.localPosition - eyelocalspace;
		eye_to_smallcube.Normalize ();

		GameObject world = GameObject.Find ("WorldView");
		GameObject world_cube = world.transform.Find ("Cubes").gameObject;
		GameObject largecube = world_cube.transform.Find (minimapcube_pre.name).gameObject;
		BoxCollider collider = largecube.GetComponent<BoxCollider> ();
		float cubesize = 1.0f;
		if (collider != null) {
			cubesize = collider.bounds.extents.x;
		}

		if (camera_root != null) {
			Debug.Log ("small:" + eye_to_smallcube);

			Vector3 cameraposition = largecube.transform.position - eye_to_smallcube * (cubesize + teleportdistance);
			Debug.Log ("pos:" + cameraposition);
			camera_root.transform.SetPositionAndRotation (cameraposition, camera_root.transform.rotation);
		}
		SteamVR_Fade.Start (Color.clear, 1.0f);
	}

	private void OnGripPressed(object sender, ControllerInteractionEventArgs e)
	{
		isGripped = true;
		CubeInteraction.SetGrabbingPanelFlag (true);
		onhoverFlag = true;
	}

	private void OnGripRelease(object sender, ControllerInteractionEventArgs e)
	{
		isGripped = false;
	}

	private void OnGrab(object sender, ObjectInteractEventArgs e)
	{
		//works both on panel and slider
		if (e.target.tag == "Slider") {
			slider = e.target.transform;
		} else if (e.target.tag == "Panel") {
			attachedObject = e.target.transform;
			attachedObject.transform.rotation = camera.transform.rotation;
			throwCoroutine = StartCoroutine (StartDeleteGestureRecognition ());

			//attachedObject.transform.SetParent (transform, true);

		}
////			GameObject rightcontroller = GameObject.Find ("RightController");
////			GameObject slider = GameObject.Find ("SmallMapSlider");
////			GameObject slider_cube = slider.transform.Find ("Slider").gameObject;
////			Vector3 controllerlocal = slider.transform.worldToLocalMatrix.MultiplyPoint (rightcontroller.transform.position);
////			Debug.Log (controllerlocal.x + ", " + controllerlocal.y + ", "+ controllerlocal.z);
////			slider_cube.transform.localPosition = new Vector3 (slider_cube.transform.localPosition.x, controllerlocal.y, slider_cube.transform.localPosition.z);
	}

	private void OnGrabRelease(object sender, ObjectInteractEventArgs e)
	{
		if (e.target.tag == "Panel") {

			Rigidbody rigidBody = e.target.GetComponent<Rigidbody> ();
			if (rigidBody != null) {
				Destroy (rigidBody);
			}
				
			if (throwCoroutine != null) {
				StopCoroutine (throwCoroutine);
				throwCoroutine = null;
			}
		}
	}

	//Panel
	private void OnHover(object sender, DestinationMarkerEventArgs e){
//		Transform hoveredObject = e.target;
//		VRTK_InteractableObject interactableObject = hoveredObject.GetComponent<VRTK_InteractableObject> ();
//
//		if (interactableObject != null && interactableObject.isGrabbable) {
//			if (interactableObject.tag == "Panel") {
//				//do every time?
////				Rigidbody rigidbody = interactableObject.GetComponent<Rigidbody>();
////				if (rigidbody != null) {
////					rigidbody.useGravity = false;
////					//rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
////					//flash
////					//Destroy(rigidbody);
////				}
//				if (attachedObject == null && isGripped) {
//					attachedObject = hoveredObject;
////					attachedObject.transform.SetParent (transform, true);
//					//attachedObject.transform.position += new Vector3 (100.0f, 0.0f, 0.0f);
//					//attachedObject.transform.rotation = camera.transform.rotation;
//
//
//					StartCoroutine (StartDeleteGestureRecognition ());
//				}
//			}
//		}
	}

	private IEnumerator StartDeleteGestureRecognition()
	{
		bool process = true;
		while (process) {
			float startOrientation = transform.rotation.eulerAngles.x;

			float accumulatedOrientation = 0.0f;
			float lastOrientation = startOrientation;

			for (int i = 0; i < 10; ++i) {
				float delta = transform.rotation.eulerAngles.x - lastOrientation;
				lastOrientation = transform.rotation.eulerAngles.x;

				if ((Mathf.Abs(delta) < 180.0f) && delta < 0.0f) {
					accumulatedOrientation += delta;

					if (accumulatedOrientation < -45.0f) {
						process = false;
						break;
					}
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		GameObject exp = Instantiate (particle, attachedObject.position, attachedObject.rotation) as GameObject;
		Destroy (attachedObject.gameObject);
		Destroy (exp, 1.2f);
		attachedObject = null;
		CubeInteraction.SetGrabbingPanelFlag (false);
		onhoverFlag = false;
	}

	void Explode(){
		


	}

	public void OnCubeUsedHilighted(object sender, DestinationMarkerEventArgs e){
		if (!onhoverFlag) {
			Transform cube = e.target;
			if (cube.tag == "LargeCubes") {
				int date = Cube_instance.GetCurrentDate ();
				Color color = SetColorandTransparency.GetOneColor (date, cube.gameObject);
				MaterialPropertyBlock props = new MaterialPropertyBlock ();
				MeshRenderer renderer;
				color.a = 1.0f;
				props.SetColor ("_Color", color);
				renderer = cube.GetComponent<MeshRenderer> ();
				renderer.SetPropertyBlock (props);
			} else if(cube.tag == "SmallCubes"){
				minimapcube = cube;
			}
		}
	}

	public void OnCubeUsedUnHilighted(object sender, DestinationMarkerEventArgs e){
		Transform cube = e.target;

		if (cube.tag == "LargeCubes") {
			int date = Cube_instance.GetCurrentDate ();
			Color color = SetColorandTransparency.GetOneColor (date, cube.gameObject);
			float transparency = SetColorandTransparency.GetOneTransparency (date, cube.gameObject);
			MaterialPropertyBlock props = new MaterialPropertyBlock ();
			MeshRenderer renderer;

			color.a = transparency;
			props.SetColor ("_Color", color);
			renderer = cube.GetComponent<MeshRenderer> ();
			renderer.SetPropertyBlock (props);
		} else if(cube.tag == "SmallCubes"){
			minimapcube = null;

		
		}
	}
}
