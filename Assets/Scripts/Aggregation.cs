﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;
using UtyMap.Unity;
using UtyMap.Unity.Data;
using UtyMap.Unity.Infrastructure.Diagnostic;
using UtyMap.Unity.Infrastructure.Primitives;
using UtyMap.Unity.Utils;
using UtyRx;

public class Aggregation : MonoBehaviour {

	public static String[,] tweet_id_month;
	public static String[,,] tweet_id_date;
	public static int[,,] aggregate_result_date;
	public static int[,] aggregate_result_month;

	void Start () {
	}

	void Update () {
	}

	public static int[,,] Aggregate_day(Vector2[] coordinates, JsonReader.Tweet[] myTweet){
		tweet_id_date= new String[31, coordinates.Length, 24];

		//longとlat逆かも？ブロックの値は適当
		//todo
		float lat_width = (coordinates [15].x - coordinates [0].x)/2.0f;
		float long_width = (coordinates [1].y - coordinates [0].y)/2.0f;
		GeoCoordinate _coordinate = new GeoCoordinate(35.632089, 139.880852);
		Vector2 new_coordinate;
		int[,,] aggregate_result_date = new int[31,coordinates.Length, 24];

		for (int i = 0; i < myTweet.Length; i++) {
			if (myTweet [i].coordinates.coordinates != null) {
				//hour
				DateTime time = System.DateTime.ParseExact(myTweet[i].created_at,
					"ddd MMM d HH':'mm':'ss zzz yyyy",
					System.Globalization.DateTimeFormatInfo.InvariantInfo,
					System.Globalization.DateTimeStyles.None);

				//coordinates
				float longtitude = float.Parse (myTweet [i].coordinates.coordinates [0]);
				float latitude = float.Parse (myTweet [i].coordinates.coordinates [1]);
				GeoCoordinate cube_coordinate = new GeoCoordinate (latitude, longtitude);
				new_coordinate = GeoUtils.ToMapCoordinate (_coordinate, cube_coordinate);

				//改良の余地
				for (int j = 0; j < coordinates.Length; j++) {
					if ((new_coordinate.x > coordinates [j].x - lat_width) && (new_coordinate.x <= coordinates [j].x + lat_width)
					    && (new_coordinate.y > coordinates [j].y - long_width) && (new_coordinate.y <= coordinates [j].y + long_width)) {
						aggregate_result_date [time.Day-1, j, time.Hour]++;

						//to get tweet information
						if (tweet_id_date [time.Day-1, j, time.Hour] == null) {
							tweet_id_date [time.Day-1, j, time.Hour] = i.ToString ();
						} else {
							tweet_id_date [time.Day-1, j, time.Hour] += "," + i.ToString ();
						}
					}
				}
			}
		}
		return aggregate_result_date;
	}

	public static int[,] Aggregate_month(Vector2[] coordinates, JsonReader.Tweet[] myTweet){
		tweet_id_month= new String[coordinates.Length, 31];

		//if you change the division interval, you have to change this number
		float lat_width = (coordinates [15].x - coordinates [0].x)/2.0f;
		float long_width = (coordinates [1].y - coordinates [0].y)/2.0f;

		GeoCoordinate _coordinate = new GeoCoordinate(35.632089, 139.880852);
		Vector2 new_coordinate;
		aggregate_result_month = new int[coordinates.Length, 31];

		for (int i = 0; i < myTweet.Length; i++) {
			if (myTweet [i].coordinates.coordinates != null) {
				
				//coordinates
				float longtitude = float.Parse (myTweet [i].coordinates.coordinates [0]);
				float latitude = float.Parse (myTweet [i].coordinates.coordinates [1]);
				GeoCoordinate cube_coordinate = new GeoCoordinate (latitude, longtitude);
				new_coordinate = GeoUtils.ToMapCoordinate (_coordinate, cube_coordinate);

				//date
				DateTime time = System.DateTime.ParseExact(myTweet[i].created_at,
					"ddd MMM d HH':'mm':'ss zzz yyyy",
					System.Globalization.DateTimeFormatInfo.InvariantInfo,
					System.Globalization.DateTimeStyles.None);

				for (int j = 0; j < coordinates.Length; j++) {
					if ((new_coordinate.x > (coordinates [j].x - lat_width)) && (new_coordinate.x <= (coordinates [j].x + lat_width))
						&& (new_coordinate.y > (coordinates [j].y - long_width)) && (new_coordinate.y <= (coordinates [j].y + long_width))) {
						aggregate_result_month [j, time.Day-1]++; //start with 0

						//to get tweet information
						if (tweet_id_month [j, time.Day - 1] == null) {
							tweet_id_month [j, time.Day - 1] = i.ToString ();
						} else {
							tweet_id_month [j, time.Day - 1] += "," + i.ToString ();
						}
					}
				}

			}
		}
		for (int j = 0; j < coordinates.Length; j++) {
			float x1 = (coordinates [j].x - lat_width);
			float x2 = coordinates [j].x + lat_width;
			float y1 = coordinates [j].y - long_width;
			float y2 = coordinates [j].y + long_width;
		}
		return aggregate_result_month;

	}

	public static String GetTweetId(int date, int i, int j){
		if(date == -1){
			return tweet_id_month[i, j];
		}else{
			return tweet_id_date [date-1, i, j];
		}
	}

}
