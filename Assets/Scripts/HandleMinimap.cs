﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class HandleMinimap : MonoBehaviour {
	private GameObject smallMap;
	public Transform cameraTransform;
	private Camera camera;
	private GameObject LeftController;

	private bool minimapShowing = true;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (LeftController == null) {
			LeftController = GameObject.Find ("LeftController");
		}

		if (smallMap == null) {
			smallMap = GameObject.FindWithTag ("SmallMap");
		}

		if (LeftController != null && smallMap != null) {
			Quaternion rotation = UnityEngine.XR.InputTracking.GetLocalRotation (UnityEngine.XR.XRNode.LeftHand);
			float zRotation = rotation.eulerAngles.z;

			if (!minimapShowing) {
				if (((zRotation > 130.0f)&&(zRotation < 230.0f)) || (zRotation < -110.0f)) {
					//smallMap.transform.position = transform.TransformPoint (InputTracking.GetLocalPosition (VRNode.LeftHand));
					smallMap.transform.SetParent (LeftController.transform, false);
					smallMap.transform.localPosition = new Vector3 (-0.03f, -0.15f, 0.07f);
					smallMap.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
					minimapShowing = true;
					smallMap.SetActive (true);
					Transform sliderset = smallMap.transform.Find ("SmallMapSlider");

					//sliderset.transform.localPosition = new Vector3 (0.0f, -0.003f, 0.0f);
				}
			} else {
				if ((zRotation < 110.0f) && (zRotation > -110.0f) || (zRotation > 250.0f))
				{
					smallMap.transform.SetParent (cameraTransform, false);
					smallMap.transform.localPosition = new Vector3 (0.0f, 0.0f, 4.0f);
					smallMap.transform.localRotation = Quaternion.identity;
					minimapShowing = false;
					smallMap.SetActive (false);
				}
			}
		}
	}
}
