﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;
using UtyMap.Unity;
using UtyMap.Unity.Data;
using UtyMap.Unity.Infrastructure.Diagnostic;
using UtyMap.Unity.Infrastructure.Primitives;
using UtyMap.Unity.Utils;
using UtyRx;

public class JsonReader : MonoBehaviour {

	[Serializable]
	public class Tweet
	{
		public string source;
		public bool retweeted;
		public bool favorited;
		public Coordinate_nest coordinates;
		public Retweeted_status retweeted_status;
		public Place place;
		public int retweet_count;
		public int favorite_count;
		public Entities entities;
		public bool truncated;
		public string created_at;
		public string in_reply_to_status_id_str;
		public string contributors;
		public string text;
		public User user;
		public string in_reply_to_user_id;
		public string id;
		public string in_reply_to_status_id;
		public Geo geo;
		public string in_reply_to_user_id_str;
		public string id_str;
		public string in_reply_to_screen_name;
	}

	[Serializable]
	public class Coordinate_nest
	{
		public string[] coordinates;
		public string type;
	}
	[Serializable]
	public class Retweeted_status{
		public string user;//あやしい

	}
	[Serializable]
	public class Place{
		public string place_type;
		public string country;
		public string country_code;
		public string name;
		public string full_name;
		public string url;
		public string id;
		public Bounding_box bounding_box;
		public string attributes;
	}
	[Serializable]
	public class Bounding_box{
		public string[] coordinates;
		public string type;
	}
	[Serializable]
	public class Entities{
		public Hash[] hashtags;
		public string[] media;
		public string[] user_mentions;
		public URLS[] urls;
	}
	[Serializable]
	public class Hash{
		public string text;
		public string indices;
	}
	[Serializable]
	public class URLS{
		public string expanded_url;
		public string url;
		public int[] indices;
	}
	[Serializable]
	public class User{
		public int friends_count;
		public string follow_request_sent;
		public string profile_image_url;
		public string profile_sidebar_fill_color;
		public string profile_background_color;
		public string notifications;
		public string url;
		public string id;
		public string following;
		public bool is_translator;
		public string screen_name;
		public string lang;
		public string location;
		public int followers_count;
		public string name;
		public int statuses_count;
		public string description;
		public string favourites_count;
		public bool profile_background_tile;
		public string listed_count;
		public string profile_link_color;
		public bool contributors_enabled;
		public string profile_sidebar_border_color;
		public string created_at;
		public string utc_offset;
		public bool verified;
		public bool show_all_inline_media;
		public string profile_background_image_url;
		//public bool protected;
		public bool default_profile;
		public string id_str;
		public string profile_text_color;
		public bool default_profile_image;
		public string time_zone;
		public bool profile_use_background_image;
		public bool geo_enabled;
	}
	[Serializable]
	public class Geo{
		public string[] coordinates;
		public string type;
	}

	void Start () {
	}
	void Update () {
	}


	//public static Tweet[] myTweet = new Tweet[489]; //oneday
	public static Tweet[] myTweet = new Tweet[23313]; //onemonth

	// read coordinate text file
	public static Tweet[] ReadJsonFile(){
		string guitxt = "";
		//FileInfo fi = new FileInfo(Application.dataPath + "/TokyoDL_20140801.json");
		FileInfo fi = new FileInfo(Application.dataPath + "/TokyoDL_201408.json"); //onemonth

		try {
			using (StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.UTF8)){
				guitxt = sr.ReadToEnd();
			}
			string[] line = guitxt.Split('\n');
			for(int i=0;i<line.Length-1;i++){
				string[] each_json = line[i].Split('\t');
				myTweet[i] = new Tweet();
				myTweet[i] = JsonUtility.FromJson<Tweet>(each_json[1]);
			}
		} catch (Exception e){
			Debug.Log(e);
		}
		return myTweet;
	}

	public static Tweet GetTweet(int tweet_id){
		return myTweet [tweet_id];
	}

	public static Tweet[] GetAllTweets(){
		return myTweet;
	}

}
