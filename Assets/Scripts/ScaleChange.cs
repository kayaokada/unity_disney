﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.UnityEventHelper;

public class ScaleChange : MonoBehaviour {
	private VRTK_Control_UnityEvents controlEvents;
	private VRTK_InteractableObject interactableObject;
	static int count = 0;
	static int scale_value = 15;

	void Start () {
		controlEvents = GetComponent<VRTK_Control_UnityEvents>();
		interactableObject = GetComponent<VRTK_InteractableObject>();
		if (controlEvents == null){
			controlEvents = gameObject.AddComponent<VRTK_Control_UnityEvents>();
		}
//		controlEvents.OnValueChanged.AddListener(HandleChange);

		VRTK_Slider slider = GetComponent<VRTK_Slider> ();
		if (slider != null) {
			slider.ValueChanged += HandleChange2;
		}
	}

	void Update () {
	}

	void HandleChange2(object sender, Control3DEventArgs e)
	{
		Debug.Log (e.value.ToString ());
		scale_value = (int)e.value;
		Cube_instance.smallcubecolor ((int)e.value);
	}

	private void HandleChange(object sender, Control3DEventArgs e)
	{
		/*GameObject[] largecubes = GameObject.FindGameObjectsWithTag ("LargeCubes");
		foreach (GameObject cube in largecubes) {
			Destroy (cube);
}*/
	}


	public static int GetCurrentValue(){
		return scale_value;
	}
}
