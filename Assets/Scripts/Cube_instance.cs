﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO; //System.IO.FileInfo, System.IO.StreamReader, System.IO.StreamWriter
using System; //Exception
//using System.Drawing;
using System.Text; //Encoding
using UtyMap.Unity;
using UtyMap.Unity.Data;
using UtyMap.Unity.Infrastructure.Diagnostic;
using UtyMap.Unity.Infrastructure.Primitives;
using UtyMap.Unity.Utils;
using UtyRx;
using UnityEngine.UI; 
using Assets.Scripts.Scenes.Import;
using System.Linq;

public class Cube_instance : MonoBehaviour {

	public GameObject cube_prefab; //need
	public GameObject plot_prefab;
	public static float smallMapWidth = 0.1f;// Meters
	public static Camera camera;
	public Transform cameraTransform_public; //need
	//public CubeInteraction cubeInteraction;
	public GameObject smallmapTemplate_public;//need

	private static GameObject largeMap;
	private static GameObject smallMap;
	private static Color[,] cube_color;
	private static Color[,,] cube_color_date;
	private static float[,] cube_transparency;
	private static float[,,] cube_transparency_date;

	static GameObject prefab;
	static Transform cameraTransform;
	static GameObject smallmapTemplate;
	static bool initialization = true;

	public delegate void OnCubeCreate();
	public static event OnCubeCreate OnCubeCreateEvent;
	static float smallMapScale;
	static int[,] aggregate_result_month;
	static int[,,] aggregate_result_date;
	static int current_date = -1;
	static JsonReader2.Data[] myTweet_new;
	static Vector2[] coordinates;

	void Start () {
		camera = cameraTransform_public.GetComponent<Camera> ();
		prefab = cube_prefab;
		cameraTransform = cameraTransform_public;
		smallmapTemplate = smallmapTemplate_public;

		coordinates = BlockReader.ReadFile ();
		JsonReader.Tweet[] myTweet = JsonReader.ReadJsonFile ();
		myTweet_new = JsonReader2.ReadJsonFile_short ();

		//allmapSlider = smallmap_Slider;

		MainScene (coordinates, myTweet);
	}

	public static void MainScene(Vector2[] coordinates, JsonReader.Tweet[] myTweet){
		aggregate_result_month = Aggregation.Aggregate_month (coordinates, myTweet);
		aggregate_result_date = Aggregation.Aggregate_day (coordinates, myTweet);

		cube_color = new Color[aggregate_result_month.GetLength(0), aggregate_result_month.GetLength(1)];
		cube_transparency = new float[aggregate_result_month.GetLength(0), aggregate_result_month.GetLength(1)];
		cube_color = SetColorandTransparency.GetColor (aggregate_result_month);
		cube_transparency = SetColorandTransparency.GetTransparency (aggregate_result_month);

		cube_color_date = new Color[31, aggregate_result_month.GetLength(0), aggregate_result_month.GetLength(1)];
		cube_transparency_date = new float[31, aggregate_result_month.GetLength(0), aggregate_result_month.GetLength(1)];
		cube_color_date = SetColorandTransparency.GetColorDate (aggregate_result_date);
		cube_transparency_date = SetColorandTransparency.GetTransparencyDate (aggregate_result_date);

		CreateCubes (coordinates, cube_color, cube_transparency);
		//CreatePlots (coordinates, myTweet);
		largeMap = GameObject.Find ("WorldView");
		GameObject cub_instance_event = GameObject.Find ("Cube_instance");
		ImportBehavior import = largeMap.GetComponentInChildren<ImportBehavior> ();
		if (import != null) {
			import.OnMapLoadEvent += MapLoad;
		}
	}

	static void MapLoad(){
		
		smallMap = Instantiate (largeMap, Vector3.zero, Quaternion.identity);

		Bounds smallMapBounds = FindBounds (smallMap);
		smallMapScale = smallMapWidth / smallMapBounds.size.x;

		smallMap.transform.localScale = new Vector3 (smallMapScale, smallMapScale, smallMapScale);
		smallMap.name = "smallMapDetail";
		smallMap.tag = "SmallMapDetail";

		SetLayerRecursive (smallMap, LayerMask.NameToLayer ("SmallMap"));
		SetSmallCubesColorandTransparency (smallMap, -1);

		if (initialization) {
			GameObject smallMapRoot = Instantiate (smallmapTemplate);
			Transform mapRoot = smallMapRoot.transform.Find ("MapRoot");
			if (mapRoot != null) {
				smallMap.transform.SetParent (mapRoot, false);
			}
			smallMapRoot.transform.SetParent (cameraTransform, false);
			smallMapRoot.transform.SetPositionAndRotation (Vector3.zero, Quaternion.identity);
		} else {
			GameObject mapRoot = GameObject.Find ("MapRoot");
			smallMap.transform.SetParent (mapRoot.transform, false);
		}
	}

	public static void smallcubecolor(int date){
		GameObject smallmapdetail = GameObject.FindGameObjectWithTag ("SmallMapDetail");
		SetSmallCubesColorandTransparencyDate (smallmapdetail, (date-1));
	}

	public static void largecubecolor(int date){
		current_date = date;
		GameObject worldview = GameObject.Find ("WorldView");
		SetLargeCubesColorandTransparencyDate (worldview, (date-1));
	}

	static void SetLayerRecursive(GameObject parent, int layer)
	{
		parent.layer = layer;

		foreach (Transform child in parent.transform) {
			SetLayerRecursive (child.gameObject, layer);
		}
	}

	static void SetSmallCubesColorandTransparency(GameObject smallMap, int date){
		Transform child;
		MaterialPropertyBlock props = new MaterialPropertyBlock();
		MeshRenderer renderer;
		GameObject child_parent = smallMap.transform.Find ("Cubes").gameObject;
		if (date != -1) {
			foreach (Transform childcube in child_parent.transform) {
				childcube.gameObject.SetActive (true);
			}
		}
		for (int i = 0; i < cube_color.GetLength (0); i++) {
			for (int j = 0; j < 31; j++) {
				
				child = child_parent.transform.Find (i.ToString() + "," + j.ToString());
				if (child) {
					GameObject childObject = child.gameObject;
					if (date == -1) {
						cube_color [i, j].a = cube_transparency [i, j];
						props.SetColor ("_Color", cube_color [i, j]);
					} else {
						if (j < 24) {
							cube_color_date [date, i, j].a = cube_transparency_date [date, i, j];
							props.SetColor ("_Color", cube_color_date [date, i, j]);
							if (cube_transparency_date [date, i, j] < 0.1f) {
								childObject.SetActive (false);
							}
						} else {
							childObject.SetActive (false);
						}
					}
					renderer = childObject.GetComponent<MeshRenderer> ();
					renderer.SetPropertyBlock (props);
					childObject.tag = "SmallCubes";
				}

			}
		}
	}

	static void SetSmallCubesColorandTransparencyDate(GameObject smallMap, int date){
		Transform child;
		MaterialPropertyBlock props = new MaterialPropertyBlock();
		MeshRenderer renderer;
		GameObject child_parent = smallMap.transform.Find ("Cubes").gameObject;
		if (date != -1) {
			foreach (Transform childcube in child_parent.transform) {
				String[] name = childcube.name.Split (',');
				int args0 = int.Parse (name [0]);
				int args1 = int.Parse (name [1]);
				if (args1 >= 24) {
					childcube.gameObject.SetActive (false);
				} else {
					if (cube_transparency_date [date, args0, args1] < 0.1f) {
						childcube.gameObject.SetActive (false);
					}else{
						childcube.gameObject.SetActive (true);
						cube_color_date [date, args0, args1].a = cube_transparency_date [date, args0, args1];
						props.SetColor ("_Color", cube_color_date [date, args0, args1]);
						renderer = childcube.gameObject.GetComponent<MeshRenderer> ();
						renderer.SetPropertyBlock (props);
						childcube.gameObject.tag = "SmallCubes";
					}
				}
			}
		}
	}

	static void SetLargeCubesColorandTransparencyDate(GameObject smallMap, int date){
		Transform child;
		MaterialPropertyBlock props = new MaterialPropertyBlock();
		MeshRenderer renderer;
		GameObject child_parent = smallMap.transform.Find ("Cubes").gameObject;
		if (date != -1) {
			foreach (Transform childcube in child_parent.transform) {
				String[] name = childcube.name.Split (',');
				int args0 = int.Parse (name [0]);
				int args1 = int.Parse (name [1]);
				if (args1 >= 24) {
					childcube.gameObject.SetActive (false);
				} else {
					if (cube_transparency_date [date, args0, args1] < 0.1f) {
						childcube.gameObject.SetActive (false);
					}else{
						childcube.gameObject.SetActive (true);
						cube_color_date [date, args0, args1].a = cube_transparency_date [date, args0, args1];
						props.SetColor ("_Color", cube_color_date [date, args0, args1]);
						renderer = childcube.gameObject.GetComponent<MeshRenderer> ();
						renderer.SetPropertyBlock (props);
						childcube.gameObject.tag = "LargeCubes";
					}
				}
			}
		}
	}

	static Bounds FindBounds(GameObject gameObject)
	{
		Bounds combinedBounds = new Bounds ();
		Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer> ();
		foreach (Renderer render in renderers) {
			combinedBounds.Encapsulate(render.bounds);
		}
		Debug.Log (combinedBounds);
		return combinedBounds;
	}

	void Update () {
	}

	//create cubes
	static void CreateCubes(Vector2[] coordinates, Color[,] cube_color, float[,] cube_transparency){
		MaterialPropertyBlock props = new MaterialPropertyBlock();
		MeshRenderer renderer;
		GameObject worldview = GameObject.Find ("WorldView");
		GameObject parentObject = worldview.transform.Find ("Cubes").gameObject;
			
		for(int i=0;i<coordinates.Length;i++){
			for(int j=0;j<cube_color.GetLength(1);j++){
				//if (cube_transparency [i,j] > 0.1f) {
				GameObject cube = Instantiate (prefab, new Vector3 (coordinates [i].x, 60 + j*60, coordinates [i].y), Quaternion.identity);
				cube.name = i.ToString () + "," + j.ToString();
				cube.tag = "LargeCubes";
				if (cube_transparency [i, j] < 0.1f) {
					cube.SetActive (false);
				}

				cube_color [i,j].a = cube_transparency [i,j];
				props.SetColor ("_Color", cube_color [i,j]);
				cube.transform.parent = parentObject.transform;
				renderer = cube.GetComponent<MeshRenderer> ();
				renderer.SetPropertyBlock (props);
				CubeInteraction.RegisterCube (cube);
			}
		}
	}

	/*public static void CreatePanels(GameObject panel, Vector3 pos, String name, GameObject cube){
		String[] cube_id = name.Split(',');
		String tweet_ids = Aggregation.GetTweetId (current_date, int.Parse(cube_id[0]),int.Parse(cube_id[1]));
		String[] tweet_ids_array = tweet_ids.Split (',');
		JsonReader.Tweet[] tweets = new JsonReader.Tweet[tweet_ids_array.Length];
		String change_text = "";
		String change_tweet_text = "";
		IDictionary<string, int> hash = new Dictionary<string, int>();
		IDictionary<string, int> tweet = new Dictionary<string, int>();
		DateTime time = DateTime.Now;
		String longtitude = "";
		String latitude = "";
		int followers_threshold;
		if (current_date == -1) {
			followers_threshold = 400;
		} else {
			followers_threshold = 200;
		}

		for(int i=0;i<tweet_ids_array.Length;i++){
			tweets[i] = JsonReader.GetTweet (int.Parse(tweet_ids_array [i]));
			if(i==0){
				time = System.DateTime.ParseExact(tweets[i].created_at,
					"ddd MMM d HH':'mm':'ss zzz yyyy",
					System.Globalization.DateTimeFormatInfo.InvariantInfo,
					System.Globalization.DateTimeStyles.None);
				longtitude = tweets [i].coordinates.coordinates [0];
				latitude = tweets [i].coordinates.coordinates [1];
			}
			DateTime user_createdat = System.DateTime.ParseExact(tweets[i].user.created_at,
				"ddd MMM d HH':'mm':'ss zzz yyyy",
				System.Globalization.DateTimeFormatInfo.InvariantInfo,
				System.Globalization.DateTimeStyles.None);
			TimeSpan span = time - user_createdat;

			//not_reply_tweets
			if (tweets [i].in_reply_to_user_id == "") {
				if ((tweets [i].user.followers_count > followers_threshold) && ((tweets [i].user.statuses_count / span.Days) > 5)) {
					if (!tweet.ContainsKey (tweets [i].text)) {
						tweet.Add (tweets [i].text, tweets [i].favorite_count + tweets [i].retweet_count);
					} else {
						if (tweet [tweets [i].text] < tweets [i].favorite_count + tweets [i].retweet_count) {
							tweet [tweets [i].text] = tweets [i].favorite_count + tweets [i].retweet_count;
						}
					}
				}
			}
		}

		int count = 1;
		count = 1;
		var sortedTweet = from entry in tweet orderby entry.Value ascending select entry;
		foreach (KeyValuePair<string, int> pair in sortedTweet) {
			String tweet_text_replace = pair.Key.Replace ('\n', ' ');
			if (tweet_text_replace.Length > 45)
				tweet_text_replace = tweet_text_replace.Substring (0, 45);
			change_tweet_text += count.ToString() + ".  " + tweet_text_replace + "\n\n";
			count++;
			if (count > 5)
				break;
		}
		//GameObject cube_panel = Instantiate (panel, new Vector3 (pos.x,pos.y, pos.z), Quaternion.identity);
		GameObject cube_panel = Instantiate (panel, new Vector3 (pos.x, pos.y,pos.z) + camera.transform.right *50 + new Vector3(0.0f, 0.0f, -30.0f), camera.transform.rotation);
		cube_panel.name = "Panel_" + name;
		cube_panel.tag = "Panel";
		Text date = cube_panel.transform.Find("DateChangeText").GetComponentInChildren<Text>();
		date.text = time.ToString("MMMM dd, yyyy");
		Text position = cube_panel.transform.Find("PosChangeText").GetComponentInChildren<Text>();
		position.text = longtitude + ", " + latitude;
		//Text text = cube_panel.transform.Find("HashTagsChangeText").GetComponentInChildren<Text>();
		//text.text = change_text;
		Text tweettext = cube_panel.transform.Find("TweetChangeText").GetComponentInChildren<Text>();
		tweettext.text = change_tweet_text;

		Color frameColor = SetColorandTransparency.GetOneColor (current_date, cube);
		Transform frame = cube_panel.transform.Find ("Frame");
		MaterialPropertyBlock props = new MaterialPropertyBlock();
		MeshRenderer renderer;
		props.SetColor ("_Color", frameColor);
		foreach (Transform childframe in frame) {
			renderer = childframe.gameObject.GetComponent<MeshRenderer> ();
			renderer.SetPropertyBlock (props);
		}

		Rigidbody rigidbody = cube_panel.GetComponent<Rigidbody>();
		if (rigidbody != null) {
			rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
		}
	}*/

	public static void CreatePanels(GameObject panel, Vector3 pos, String name, GameObject cube){
		String[] cube_id = name.Split(',');
		String tweet_ids = Aggregation.GetTweetId (current_date, int.Parse(cube_id[0]),int.Parse(cube_id[1]));
		String[] tweet_ids_array = tweet_ids.Split (',');

		JsonReader2.Tweet[] tweets = new JsonReader2.Tweet[tweet_ids_array.Length];
		String change_text = "";
		String change_tweet_text = "";
		DateTime time = DateTime.Now;
		String longtitude = "";
		String latitude = "";
		String tweet_text_replace = "";

		for(int i=0;i<myTweet_new[28].tweet.Length;i++){
			DateTime user_createdate = System.DateTime.ParseExact(myTweet_new[28].tweet[i].created_at,
				"yyyy-MM-dd HH':'mm':'ss",
				System.Globalization.DateTimeFormatInfo.InvariantInfo,
				System.Globalization.DateTimeStyles.None);
			if(user_createdate.Date.ToString().Contains("8/22") || user_createdate.Date.ToString().Contains("8/24")|| user_createdate.Date.ToString().Contains("8/25")|| user_createdate.Date.ToString().Contains("8/26")){
				time = System.DateTime.ParseExact(myTweet_new[28].tweet[i].created_at,
					"yyyy-MM-dd HH':'mm':'ss",
					System.Globalization.DateTimeFormatInfo.InvariantInfo,
					System.Globalization.DateTimeStyles.None);
				if (myTweet_new [28].tweet [i].text.Length > 45)
					tweet_text_replace = myTweet_new [28].tweet [i].text.Substring (0, 45);
				change_tweet_text += tweet_text_replace + "\n\n";
				longtitude = myTweet_new [28].tweet [i].coordinate[0].ToString();
				latitude = myTweet_new [28].tweet [i].coordinate[1].ToString();
			}
		}

		/*int count = 1;
		var sortedTweet = from entry in tweet orderby entry.Value ascending select entry;
		foreach (KeyValuePair<string, int> pair in sortedTweet) {
			String tweet_text_replace = pair.Key.Replace ('\n', ' ');
			if (tweet_text_replace.Length > 45)
				tweet_text_replace = tweet_text_replace.Substring (0, 45);
			change_tweet_text += count.ToString() + ".  " + tweet_text_replace + "\n\n";
			count++;
			if (count > 5)
				break;
		}*/
		GameObject cube_panel = Instantiate (panel, new Vector3 (pos.x, pos.y,pos.z) + camera.transform.right *50 + new Vector3(0.0f, 0.0f, -30.0f), camera.transform.rotation);
		cube_panel.name = "Panel_" + name;
		cube_panel.tag = "Panel";
		Text date = cube_panel.transform.Find("DateChangeText").GetComponentInChildren<Text>();
		date.text = time.ToString("MMMM dd, yyyy");
		Text position = cube_panel.transform.Find("PosChangeText").GetComponentInChildren<Text>();
		position.text = longtitude + ", " + latitude;
		Text tweettext = cube_panel.transform.Find("TweetChangeText").GetComponentInChildren<Text>();
		tweettext.text = change_tweet_text;

		Color frameColor = SetColorandTransparency.GetOneColor (current_date, cube);
		Transform frame = cube_panel.transform.Find ("Frame");
		MaterialPropertyBlock props = new MaterialPropertyBlock();
		MeshRenderer renderer;
		props.SetColor ("_Color", frameColor);
		foreach (Transform childframe in frame) {
			renderer = childframe.gameObject.GetComponent<MeshRenderer> ();
			renderer.SetPropertyBlock (props);
		}

		Rigidbody rigidbody = cube_panel.GetComponent<Rigidbody>();
		if (rigidbody != null) {
			rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
		}
	}

	void CreatePlots(Vector2[] coordinates, JsonReader.Tweet[] myTweet){
		GeoCoordinate _coordinate = new GeoCoordinate(35.632089, 139.880852);
		Vector2 new_coordinate;
		MaterialPropertyBlock props = new MaterialPropertyBlock();
		MeshRenderer renderer;

		for (int i = 0; i < myTweet.Length; i++) {
			if (myTweet [i].coordinates.coordinates != null) {
				//coordinates
				float longtitude = float.Parse (myTweet [i].coordinates.coordinates [0]);
				float latitude = float.Parse (myTweet [i].coordinates.coordinates [1]);
				GeoCoordinate cube_coordinate = new GeoCoordinate (latitude, longtitude);
				new_coordinate = GeoUtils.ToMapCoordinate (_coordinate, cube_coordinate);

				DateTime time = System.DateTime.ParseExact(myTweet[i].created_at,
					"ddd MMM d HH':'mm':'ss zzz yyyy",
					System.Globalization.DateTimeFormatInfo.InvariantInfo,
					System.Globalization.DateTimeStyles.None);
				GameObject plots = Instantiate (plot_prefab, new Vector3 (new_coordinate.x, 60 + time.Day*60 + time.Hour*60/24, new_coordinate.y), Quaternion.identity);
				/*cube.name = i.ToString () + "," + j.ToString();
				cube_color [i,j].a = cube_transparency [i,j];
				props.SetColor ("_Color", cube_color [i,j]);*/
				renderer = plots.GetComponent<MeshRenderer> ();
				renderer.SetPropertyBlock (props);
			}
		}
	}

	public static int GetCurrentDate(){
		return current_date;
	}

}