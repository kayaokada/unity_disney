﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineToMap : MonoBehaviour {
	private LineRenderer line;
	private GameObject mickey;
	public Material linematerial;

	public float mickeyMinScale = 1.0f;
	public float mickeyMaxScale = 20.0f;
	public float mickeyMaxHeight = 300.0f;
	private float buildingHeight = 20.0f;

	// Use this for initialization
	void Start () {
		line = gameObject.AddComponent<LineRenderer>();
		line.SetVertexCount (2);
		line.startWidth = 0.5f;
		line.endWidth = 0.5f;
		line.material = linematerial;
		Color colour = new Color (0.0f, 0.0f, 1.0f);
		line.startColor = colour;
		line.endColor = colour;
		Vector3 startvec = gameObject.transform.position;
		Vector3 endvec = new Vector3 (gameObject.transform.position.x, 0.0f, gameObject.transform.position.z);
		line.SetPosition (0, startvec);
		line.SetPosition (1, endvec);

		mickey = GameObject.FindWithTag ("Mickey");
		mickey.transform.position = new Vector3 (gameObject.transform.position.x, 0.0f, gameObject.transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 startvec = gameObject.transform.position - Vector3.down * 0.5f - gameObject.transform.forward * 0.5f;
		Vector3 endvec = new Vector3 (gameObject.transform.position.x, 0.0f, gameObject.transform.position.z);
		line.SetPosition (0, startvec);
		line.SetPosition (1, endvec);

		float raycastHeight = Mathf.Min (gameObject.transform.position.y - 3.0f, buildingHeight);

		RaycastHit hit;
		if (Physics.Raycast (endvec + Vector3.up * raycastHeight, Vector3.down, out hit, buildingHeight, 11)) {
			mickey.transform.position = hit.point;
		}

		float scale = Mathf.Lerp(mickeyMinScale, mickeyMaxScale, Mathf.Clamp(gameObject.transform.position.y / mickeyMaxHeight, 0.0f, 1.0f));
		mickey.transform.localScale = new Vector3 (scale, scale, scale);
		mickey.transform.rotation = Quaternion.Euler(new Vector3(0.0f, gameObject.transform.rotation.eulerAngles.y, 0.0f));
		mickey.transform.position += mickey.transform.forward * Mathf.Lerp (0.0f, 8.0f, 1.0f - Mathf.Clamp (gameObject.transform.position.y / buildingHeight, 0.0f, 1.0f));

	}
}
