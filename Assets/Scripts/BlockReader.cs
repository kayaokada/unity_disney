﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;
using UtyMap.Unity;
using UtyMap.Unity.Data;
using UtyMap.Unity.Infrastructure.Diagnostic;
using UtyMap.Unity.Infrastructure.Primitives;
using UtyMap.Unity.Utils;
using UtyRx;


public class BlockReader : MonoBehaviour {

	//todo
	public static Vector2[] coordinates = new Vector2[500];

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// read coordinate text file
	public static Vector2[] ReadFile(){
		string guitxt = "";
		FileInfo fi = new FileInfo(Application.dataPath + "/" + "blocks_onlydisney.txt");
		try {
			using (StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.UTF8)){
				guitxt = sr.ReadToEnd();
			}
			string[] words = guitxt.Split('\n');
			string[] coordinate = new String[3];
			Array.Resize(ref coordinates, (int)(words.Length/4));

			//center of the map
			GeoCoordinate _coordinate = new GeoCoordinate(35.632089, 139.880852);

			float longtitude_first_corner = 0.0f;
			float latitude_first_corner = 0.0f;
			float longtitude_third_corner = 0.0f;
			float latitude_third_corner = 0.0f;
			float longtitude = 0.0f;
			float latitude = 0.0f;

			for(int i=0;i<words.Length-1;i++){
				if(i % 4 == 0){
					//first corner
					coordinate = words[i].Split(' ');
					coordinate[2] = coordinate[2].Replace("]", "");
					longtitude_first_corner = float.Parse(coordinate[1]);
					latitude_first_corner = float.Parse(coordinate[2]);
				}else if(i % 4 == 2){
					//third corner
					coordinate = words[i].Split(' ');
					coordinate[2] = coordinate[2].Replace("]", "");
					longtitude_third_corner = float.Parse(coordinate[1]);
					latitude_third_corner = float.Parse(coordinate[2]);

					//set the center of each cube
					longtitude = longtitude_first_corner + (longtitude_third_corner - longtitude_first_corner) / 2.0f;
					latitude = latitude_first_corner + (latitude_third_corner - latitude_first_corner) / 2.0f;
					GeoCoordinate cube_coordinate = new GeoCoordinate(latitude, longtitude);
					coordinates[(int)(i/4)] = GeoUtils.ToMapCoordinate(_coordinate, cube_coordinate);
				}
			}
		} catch (Exception e){
			Debug.Log(e);
		}
		return coordinates;
	}

	public static Vector2[] GetCoordinates(){
		return coordinates;
	}
}
