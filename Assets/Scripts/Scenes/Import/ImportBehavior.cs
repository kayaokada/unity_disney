﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Core;
using Assets.Scripts.Core.Plugins;
using UnityEngine;
using UtyMap.Unity;
using UtyMap.Unity.Data;
using UtyMap.Unity.Infrastructure.Diagnostic;
using UtyMap.Unity.Infrastructure.Primitives;
using UtyMap.Unity.Utils;
using UtyRx;
using Component = UtyDepend.Component;

namespace Assets.Scripts.Scenes.Import
{
    /// <summary> Scene behaviour which demonstrates how to import data manually. </summary>
    internal class ImportBehavior: MonoBehaviour
    {
        /// <summary> Demonstrates two types of scene. </summary>
        public enum SceneType
        {
            Bird,
            Street
        }

		public delegate void OnMapLoad();

		public event OnMapLoad OnMapLoadEvent;

        /// <summary> Path format to map data on disk. </summary>
        /// <remarks> 
        ///    In general, MapCss should adopt tag naming differences for different map data formats,
        ///    e.g OSM xml schema VS mapzen geojson.
        ///    So, assume that you're using default mapcss. Then, if you import map data from OSM, you
        ///    should use LOD = 16, if from mapzen geojson: from 1 to 15. For details, compare mapcss
        ///    rules inside default mapcss.
        /// </remarks>
//        private const string MapDataPathFormat = @"../../../../core/test/test_assets/osm/berlin.osm.{0}";
		private const string MapDataPathFormat = @"core/test/test_assets/osm/cinderella_1.osm.{0}";

        /// <summary> Start coordinate: Unity's world zero point. </summary>
        /// <remarks>
        ///    If coordinate is not inside imported map data, then data will be fetched from remote server
        ///    based on level of detail.
        /// </remarks>
//        private readonly GeoCoordinate _coordinate = new GeoCoordinate(52.5317429, 13.3871987);
		private readonly GeoCoordinate _coordinate = new GeoCoordinate(35.632089, 139.880852);
		private readonly GeoCoordinate _coordinate_north = new GeoCoordinate(35.635565, 139.879276);
		private readonly GeoCoordinate _coordinate_west = new GeoCoordinate(35.632548, 139.877195);
		private readonly GeoCoordinate _coordinate_east = new GeoCoordinate(35.633149, 139.886086);
		private readonly GeoCoordinate _coordinate_south = new GeoCoordinate(35.629985, 139.881723);
		private readonly GeoCoordinate _coordinate_north_west = new GeoCoordinate(35.635730, 139.875475);
		private readonly GeoCoordinate _coordinate_north_east = new GeoCoordinate(35.635839, 139.885313);
		private readonly GeoCoordinate _coordinate_south_west = new GeoCoordinate(35.628614, 139.875788);
		private readonly GeoCoordinate _coordinate_south_east = new GeoCoordinate(35.627881, 139.886978);

        /// <summary> Type of the scene. </summary>
        public SceneType Scene = SceneType.Street;

        private CompositionRoot _compositionRoot;
        private IMapDataStore _mapDataStore;

        void Start()
        {
            // init utymap library
            _compositionRoot = InitTask.Run((container, config) =>
            {
                container
					.Register(Component.For<Stylesheet>().Use<Stylesheet>(@"mapcss/default/index.mapcss"))
//                    .Register(Component.For<Stylesheet>().Use<Stylesheet>(@"mapcss/default/index.mapcss"))
                    .Register(Component.For<MaterialProvider>().Use<MaterialProvider>())
                    .Register(Component.For<GameObjectBuilder>().Use<GameObjectBuilder>())
                    .RegisterInstance<IEnumerable<IElementBuilder>>(new List<IElementBuilder>());
            });
            // store map data store reference to member variable
            _mapDataStore = _compositionRoot.GetService<IMapDataStore>();

            // for demo purpose, disable mesh caching to force import data into memory for every run
            _compositionRoot.GetService<IMapDataLibrary>().DisableCache();

			string mydatapathformat = System.IO.Path.Combine(Application.streamingAssetsPath, MapDataPathFormat);

            // get reference for active stylesheet.
            var stylesheet = _compositionRoot.GetService<Stylesheet>();
            // get reference to trace.
            var trace = _compositionRoot.GetService<ITrace>();
            // create tile which represents target region to load.
            var tile = new Tile(
                // create quadkey using coordinate and choose proper LOD
                GeoUtils.CreateQuadKey(_coordinate, Scene == SceneType.Bird ? 14 : 16),
                // provide stylesheet (used to be the same as for import)
                stylesheet,
                // use cartesian projection as we want to build flat world
                new CartesianProjection(_coordinate),
                // use flat elevation (all vertices have zero meters elevation)
                ElevationDataType.Flat,
                // parent for built game objects
                gameObject);

			var tile_north = new Tile(
				GeoUtils.CreateQuadKey(_coordinate_north, Scene == SceneType.Bird ? 14 : 16),
				stylesheet,
				new CartesianProjection(_coordinate),
				ElevationDataType.Flat,
				gameObject);

			var tile_west = new Tile(
				GeoUtils.CreateQuadKey(_coordinate_west, Scene == SceneType.Bird ? 14 : 16),
				stylesheet,
				new CartesianProjection(_coordinate),
				ElevationDataType.Flat,
				gameObject);

			var tile_east = new Tile(
				GeoUtils.CreateQuadKey(_coordinate_east, Scene == SceneType.Bird ? 14 : 16),
				stylesheet,
				new CartesianProjection(_coordinate),
				ElevationDataType.Flat,
				gameObject);

			var tile_south = new Tile(
				GeoUtils.CreateQuadKey(_coordinate_south, Scene == SceneType.Bird ? 14 : 16),
				stylesheet,
				new CartesianProjection(_coordinate),
				ElevationDataType.Flat,
				gameObject);

			var tile_north_west = new Tile(
				GeoUtils.CreateQuadKey(_coordinate_north_west, Scene == SceneType.Bird ? 14 : 16),
				stylesheet,
				new CartesianProjection(_coordinate),
				ElevationDataType.Flat,
				gameObject);

			var tile_north_east = new Tile(
				GeoUtils.CreateQuadKey(_coordinate_north_east, Scene == SceneType.Bird ? 14 : 16),
				stylesheet,
				new CartesianProjection(_coordinate),
				ElevationDataType.Flat,
				gameObject);

			var tile_south_west = new Tile(
				GeoUtils.CreateQuadKey(_coordinate_south_west, Scene == SceneType.Bird ? 14 : 16),
				stylesheet,
				new CartesianProjection(_coordinate),
				ElevationDataType.Flat,
				gameObject);

			var tile_south_east = new Tile(
				GeoUtils.CreateQuadKey(_coordinate_south_east, Scene == SceneType.Bird ? 14 : 16),
				stylesheet,
				new CartesianProjection(_coordinate),
				ElevationDataType.Flat,
				gameObject);

            // import data into memory
            _mapDataStore.Add(
                // define where geoindex is created (in memory, not persistent)
                MapDataStorageType.InMemory,
                // path to map data
				String.Format(mydatapathformat, Scene == SceneType.Bird ? "json" : "xml"),
                // stylesheet is used to import only used data and skip unused
                stylesheet,
                // level of detail (zoom) for which map data should be imported
                new Range<int>(16, 16))
                // start import and listen for events.
                .Subscribe(
                    // NOTE progress callback is ignored
                    (progress) => { },
                    // exception is reported
                    (exception) => trace.Error("import", exception, "Cannot import map data"),
                    // once completed, load the corresponding tile
//                    () => _mapDataStore.OnNext(tile));
					() => OnDataImported(tile, tile_north, tile_west, tile_east, tile_south, tile_north_west, tile_north_east, tile_south_west, tile_south_east));
			
        }

		private void OnDataImported(Tile tile, Tile tile_north, Tile tile_west, Tile tile_east, Tile tile_south, Tile tile_north_west, Tile tile_north_east, Tile tile_south_west, Tile tile_south_east)
		{
//			AddElement();
			LoadTile(tile, tile_north, tile_west, tile_east, tile_south, tile_north_west, tile_north_east, tile_south_west, tile_south_east);
			if (OnMapLoadEvent != null) {
				OnMapLoadEvent ();
			}
		}

		/// <summary> Start loading of the tile. </summary>
		private void LoadTile(Tile tile, Tile tile_north, Tile tile_west, Tile tile_east, Tile tile_south, Tile tile_north_west, Tile tile_north_east, Tile tile_south_west, Tile tile_south_east)
		{
			_mapDataStore.OnNext(tile);
			_mapDataStore.OnNext(tile_north);
			_mapDataStore.OnNext(tile_west);
			_mapDataStore.OnNext(tile_east);
			_mapDataStore.OnNext(tile_south);
			_mapDataStore.OnNext(tile_north_west);
			_mapDataStore.OnNext(tile_north_east);
			_mapDataStore.OnNext(tile_south_west);
			_mapDataStore.OnNext(tile_south_east);
		}
    }
}
