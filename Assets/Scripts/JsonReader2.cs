﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;
using UtyMap.Unity;
using UtyMap.Unity.Data;
using UtyMap.Unity.Infrastructure.Diagnostic;
using UtyMap.Unity.Infrastructure.Primitives;
using UtyMap.Unity.Utils;
using UtyRx;

public class JsonReader2 : MonoBehaviour {

	[Serializable]
	public class Data
	{
		public int block;
		public Tweet[] tweet;
	}
	[Serializable]
	public class Tweet{
		public int user_id;
		public string name;
		public int followers;
		public int followings;
		public int statuses_count;
		public string created_at;
		public string[] coordinate;
		public string text;
	}
	void Start () {
	}
	void Update () {
	}
		
	public static Data[] myTweet_new = new Data[72];
	public static Data[] ReadJsonFile_short(){
		string guitxt = "";
		FileInfo fi = new FileInfo(Application.dataPath + "/selected_tweets_new_2014_8.json");
		try {
			using (StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.UTF8)){
				guitxt = sr.ReadToEnd();
			}
			string[] line = guitxt.Split('\n');
			for(int i=0;i<line.Length;i++){
				myTweet_new[i] = new Data();
				myTweet_new[i] = JsonUtility.FromJson<Data>(line[i]);
			}
		} catch (Exception e){
			Debug.Log(e);
		}
		return myTweet_new;
	}

}
