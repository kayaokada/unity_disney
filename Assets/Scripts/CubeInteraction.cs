﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class CubeInteraction : MonoBehaviour {
	public GameObject panel_prefab;
	private static bool flag;
	static GameObject panel;

	// Use this for initialization
	void Start () {
		panel = panel_prefab;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void RegisterCube(GameObject cube){
		VRTK_InteractableObject interactable = cube.GetComponent<VRTK_InteractableObject> ();
		if (interactable != null) {
			interactable.InteractableObjectUsed += OnCubeUsed;
		}
	}

	public static void OnCubeUsed(object sender, InteractableObjectEventArgs e){
		if (flag == false) {
			VRTK_InteractableObject senderComponent = (VRTK_InteractableObject)sender;
			Vector3 objectPos = senderComponent.gameObject.transform.position;
			string objectName = senderComponent.gameObject.name;
			if (GameObject.Find ("Panel_" + objectName) == null) {
				Cube_instance.CreatePanels (panel, objectPos, objectName, senderComponent.gameObject);
			}
		}

	}

	public static void SetGrabbingPanelFlag(bool f){
		flag = f;
	}
}
